from typing import Iterable, Tuple

from .generator import StudentsListDocumentGenerator
from ..models import Student


def export_students_list(students: Iterable[Student], file_format: str) -> Tuple[str, str]:
    data, file_name = StudentsListDocumentGenerator(students, file_format).get_data()
    return data, file_name
