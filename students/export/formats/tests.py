from unittest import mock

from ...models import Student
from .csv import CSVStudentsExporter


def test_csv_exporter_get_row():
    student = {'first_name': 'John', 'last_name': 'Doe', 'courses_count': 4, 'completed_courses': 3}
    exporter = CSVStudentsExporter([student])
    actual_result = exporter.get_row(student)
    expected_result = "John, Doe, 4, 3"

    assert actual_result == expected_result


def test_csv_exporter_get_row_with_zero_completed_courses():
    student = {'first_name': 'John', 'last_name': 'Doe', 'courses_count': 4, 'completed_courses': 0}
    exporter = CSVStudentsExporter([student])
    actual_result = exporter.get_row(student)
    expected_result = "John, Doe, 4, 0"
    assert actual_result == expected_result


def test_csv_exporter_get_row_with_empty_data():
    student = {'first_name': '', 'last_name': '', 'courses_count': '', 'completed_courses': ''}
    exporter = CSVStudentsExporter([student])
    actual_result = exporter.get_row(student)
    expected_result = ", , , "
    assert actual_result == expected_result


def test_csv_exporter_get_row_with_null_data():
    student = {'first_name': None, 'last_name': None, 'courses_count': None, 'completed_courses': None}
    exporter = CSVStudentsExporter([student])
    actual_result = exporter.get_row(student)
    expected_result = ", , , "
    assert actual_result == expected_result


def test_csv_exporter_get_row_with_false_bool_data():
    student = {'first_name': 'John', 'last_name': 'Doe', 'courses_count': False, 'completed_courses': 0}
    exporter = CSVStudentsExporter([student])
    actual_result = exporter.get_row(student)
    expected_result = "John, Doe, False, 0"
    assert actual_result == expected_result


@mock.patch('students.export.formats.base.get_student_assigned_courses_count', return_value=3)
@mock.patch('students.export.formats.base.get_student_completed_courses_count', return_value=2)
def test_csv_exporter_get_data(assigned_courses_count, completed_courses_count):
    students = (
        Student(first_name="John", last_name="Doe"),
        Student(first_name="Dohn", last_name="Joe"),
    )
    exporter = CSVStudentsExporter(students)
    actual_result = exporter.get_data()
    heading_line = exporter.make_row(exporter.headings)
    expected_result = f"{heading_line}\n" \
                      "John, Doe, 3, 2\n" \
                      "Dohn, Joe, 3, 2\n"
    assert actual_result == expected_result


def test_csv_exporter_get_data_no_rows():
    students = []
    exporter = CSVStudentsExporter(students)
    actual_result = exporter.get_data()
    heading_line = exporter.make_row(exporter.headings)
    expected_result = f"{heading_line}\n"
    assert actual_result == expected_result
