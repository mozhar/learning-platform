from .base import BaseStudentsListExporter


class CSVStudentsExporter(BaseStudentsListExporter):
    headings = ('First name', "Last name", "Assigned courses", "Completed courses")

    def get_data(self):
        data = f'{self.make_row(self.headings)}\n'
        for student in self.students:
            context = self.get_context(student)
            data += f'{self.get_row(context)}\n'
        return data

    def get_row(self, context):
        return self.make_row(
            [context.get(field)
             for field in self.export_fields]
        )

    @classmethod
    def make_row(cls, *values):
        values_repr = list(map(lambda x: cls._get_value_representation_for_row(x), *values))
        return ', '.join(values_repr)

    @staticmethod
    def _get_value_representation_for_row(val):
        return str(val) if val is not None else ""


# class CSVnew:
#     def get_data(self):
