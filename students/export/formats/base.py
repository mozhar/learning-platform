from abc import abstractmethod
from ...services import get_student_assigned_courses_count, get_student_completed_courses_count


class BaseStudentsListExporter:
    export_fields = ('first_name', 'last_name', 'courses_count', 'completed_courses')

    def __init__(self, students):
        self.students = students

    @abstractmethod
    def get_data(self):
        pass

    def get_context(self, student):
        return {
            'first_name': student.first_name,
            'last_name': student.last_name,
            'courses_count': get_student_assigned_courses_count(student),
            'completed_courses': get_student_completed_courses_count(student),
        }

