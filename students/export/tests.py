import re
from unittest import mock

import pytest

from ..models import Student
from .exceptions import FormatNotSupportedException
from .generator import StudentsListDocumentGenerator

CSV_FILE_NAME_PATTERN = r"students_\d{4}-\d{2}-\d{2}.csv"


def _validate_csv_file_name(name):
    pattern = re.compile(CSV_FILE_NAME_PATTERN)
    return pattern.match(name)


def test_students_list_document_generator_csv_instantiated_normally():
    students = [mock.Mock]
    generator = StudentsListDocumentGenerator(students, StudentsListDocumentGenerator.CSV)
    assert generator


def test_students_list_document_generator_unsupported_format():
    students = [mock.Mock]
    with pytest.raises(FormatNotSupportedException):
        generator = StudentsListDocumentGenerator(students, "unsupported-file-format")


@mock.patch('students.export.formats.base.get_student_assigned_courses_count', return_value=3)
@mock.patch('students.export.formats.base.get_student_completed_courses_count', return_value=2)
def test_students_list_document_generator_csv(assigned_courses_count, completed_courses_count):
    students = [Student(), Student()]
    generator = StudentsListDocumentGenerator(students, StudentsListDocumentGenerator.CSV)
    data, _ = generator.get_data()
    assert len(data)
    assert isinstance(data, str)


@mock.patch('students.export.formats.base.get_student_assigned_courses_count', return_value=3)
@mock.patch('students.export.formats.base.get_student_completed_courses_count', return_value=2)
def test_students_list_document_generator_csv_file_name(assigned_courses_count, completed_courses_count):
    students = [Student()]
    generator = StudentsListDocumentGenerator(students, StudentsListDocumentGenerator.CSV)
    _, file_name = generator.get_data()
    assert _validate_csv_file_name(file_name)


@mock.patch('students.export.formats.base.get_student_assigned_courses_count', return_value=3)
@mock.patch('students.export.formats.base.get_student_completed_courses_count', return_value=2)
def test_students_list_document_generator_csv_no_students(assigned_courses_count, completed_courses_count):
    students = []
    generator = StudentsListDocumentGenerator(students, StudentsListDocumentGenerator.CSV)
    data, _ = generator.get_data()
    assert len(data)
    assert isinstance(data, str)
