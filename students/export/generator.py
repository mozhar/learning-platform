from common.utils import get_current_date_string
from .exceptions import FormatNotSupportedException

from .formats import csv


class StudentsListDocumentGenerator:
    CSV = 'csv'

    GENERATORS = {
        CSV: csv.CSVStudentsExporter,
    }

    def __init__(self, students, file_format):
        self.students = students
        self.file_format = file_format
        self.exporter = self._get_exporter()(students)

    def get_data(self):
        content = self.exporter.get_data()
        file_name = self.get_file_name()
        return content, file_name

    def get_file_name(self):
        date = get_current_date_string()
        return f"students_{date}.csv"

    def _get_exporter(self):
        generator = self.GENERATORS.get(self.file_format)
        if generator:
            return self.GENERATORS.get(self.file_format)
        raise FormatNotSupportedException(f"Format {self.file_format} is not supported.")


