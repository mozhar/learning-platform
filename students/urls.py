from django.urls import path

from students.views import StudentsListExportView

urlpatterns = [
    # path('export/', StudentsListExportView.as_view()),
    path('export/<file_format>/', StudentsListExportView.as_view(), name='export_students'),
]