import csv
import io

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from students.services import get_student_model


Student = get_student_model()


class StudentsListFileExportAPITestCase(APITestCase):
    def setUp(self):
        self.student_1 = Student.objects.create(first_name="John", last_name="Doe")
        self.student_2 = Student.objects.create(first_name="Dohn", last_name="Joe")
        self.student_3 = Student.objects.create(first_name="Phoebe", last_name="Buffay")
        self.export_url_no_format = '/students/export/'

    def test_export_csv(self):
        url = reverse('export_students', kwargs={'file_format': 'csv'})
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK

    def test_export_csv_result_is_csv(self):
        url = reverse('export_students', kwargs={'file_format': 'csv'})
        response = self.client.get(url)
        content = self._parse_streaming_content(response)
        cvs_reader = csv.reader(io.StringIO(content))
        body = list(cvs_reader)
        assert content
        assert body

    def test_export_unsupported_format(self):
        url = reverse('export_students', kwargs={'file_format': 'unsupported-format'})
        response = self.client.get(url)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_export_with_no_format(self):
        response = self.client.get(self.export_url_no_format)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @staticmethod
    def _parse_streaming_content(response):
        return "".join(map(lambda x: x.decode('utf-8'), response.streaming_content))
