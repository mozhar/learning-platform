from django.http import HttpResponseBadRequest
from rest_framework.views import APIView

from common.utils import get_file_response
from .export.exceptions import FormatNotSupportedException
from .export.services import export_students_list
from .services import get_students_queryset


class StudentsListExportView(APIView):
    queryset = get_students_queryset().prefetch_related('courseparticipant_set')

    def get(self, request, file_format, format=None):
        if not file_format:
            return HttpResponseBadRequest("No file format given")

        students = self.queryset.all()
        try:
            file_content, file_name = export_students_list(students, file_format)
            return get_file_response(file_content, file_name)
        except FormatNotSupportedException as e:
            return HttpResponseBadRequest(e)

