from django.db import models


class Student(models.Model):
    first_name = models.CharField(max_length=128, verbose_name='First name')
    last_name = models.CharField(max_length=128, verbose_name='Last name')
    email = models.EmailField()

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'
        db_table = 'student'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'
