from typing import Type

from .models import Student
from .serializers import StudentSerializer


def get_student_model() -> Type[Student]:
    return Student


def get_student_serializer():
    return StudentSerializer


def get_students_queryset():
    return Student.objects.all()


def get_student_assigned_courses_count(student: Student) -> int:
    return student.courseparticipant_set.count()


def get_student_completed_courses_count(student: Student) -> int:
    return student.courseparticipant_set.filter(completed=True).count()
