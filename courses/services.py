from typing import Iterable

from django.db.models import Count

from common.typing import ObjectId

from .models import CourseParticipant, Course

from students.services import get_student_model

Student = get_student_model()


def add_participant_to_course(course_id: ObjectId, student_id: ObjectId) -> CourseParticipant:
    return CourseParticipant.objects.create(
        course_id=course_id, student_id=student_id, completed=False
    )


def get_courses_with_students_count() -> Iterable[Course]:
    return Course.objects.annotate(students_count=Count('participants'))


def is_participant_of_course(student_id: ObjectId, course_id: ObjectId) -> bool:
    return CourseParticipant.objects.filter(course_id=course_id, student_id=student_id).exists()
