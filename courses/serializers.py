from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from . import messages
from .models import Course

from students.services import get_student_serializer, get_students_queryset

StudentSerializer = get_student_serializer()


class CourseSerializer(serializers.ModelSerializer):
    participants = StudentSerializer(many=True, read_only=True)
    participants_ids = serializers.PrimaryKeyRelatedField(
        source='participants', queryset=get_students_queryset(),
        write_only=True, many=True
    )

    class Meta:
        model = Course
        fields = ('id', 'name', 'description', 'start_date', 'end_date', 'participants', 'participants_ids')

    def validate(self, data):
        data = super().validate(data)
        self._validate_dates(data)
        return data

    def _validate_dates(self, data):
        start_date = data.get("start_date")
        end_date = data.get("end_date")
        if start_date and end_date and start_date >= end_date:
            raise ValidationError({"end_date": messages.END_DATE_BEFORE_START_DATE})


class CoursesListSerializer(serializers.ModelSerializer):
    students_count = serializers.SerializerMethodField()

    def get_students_count(self, obj):
        return obj.participants.count()

    class Meta:
        model = Course
        fields = ('name', 'start_date', 'end_date', 'students_count')
