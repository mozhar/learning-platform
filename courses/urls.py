from rest_framework.routers import DefaultRouter

from .views import CoursesViewSet

router = DefaultRouter()
router.register(r'', CoursesViewSet, basename='course')
urlpatterns = router.urls
