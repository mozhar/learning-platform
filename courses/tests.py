from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase

from students.services import get_student_model

from . import messages
from .models import Course, CourseParticipant
from .services import is_participant_of_course


Student = get_student_model()


class CourseAPITestCase(APITestCase):
    def setUp(self):
        self.course_1 = Course.objects.create(name="Advanecd Physics", start_date="2018-01-03", end_date="2018-03-03")
        self.course_2 = Course.objects.create(name="C++ in 21 day", start_date="2019-04-01", end_date="2021-04-01")
        self.student_1 = Student.objects.create(first_name="John", last_name="Doe")
        self.student_2 = Student.objects.create(first_name="Dohn", last_name="Joe")
        self.student_3 = Student.objects.create(first_name="Phoebe", last_name="Buffay")
        self.courses_url = '/courses/'  # fixme reverse('course') doesn't work fsr
        self.courses_detail_url = '/courses/{id}/'

    def test_cannot_create_course_through_api(self):
        data = {'name': 'Advanced Music Theory', 'start_date': '2019-07-14', 'end_date': '2019-10-14'}
        response = self.client.post(self.courses_url, data=data, format="json")
        course_exists = Course.objects.filter(name='Advanced Music Theory').exists()
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
        assert not course_exists

    def test_update_course(self):
        data = {'name': 'C++ in 2 years'}
        url = self.courses_detail_url.format(id=self.course_2.id)
        response = self.client.patch(url, data=data, format="json")
        updated_course = Course.objects.get(id=self.course_2.id)
        assert response.status_code == status.HTTP_200_OK
        assert updated_course.name == data["name"]

    def test_update_course_with_invalid_data(self):
        data = {'start_date': '2019-01-01', 'end_date': '2018-02-02'}
        url = self.courses_detail_url.format(id=self.course_2.id)
        response = self.client.patch(url, data=data, format="json")
        updated_course = Course.objects.get(id=self.course_2.id)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert updated_course.start_date != data["start_date"]
        assert updated_course.end_date != data["end_date"]
        assert messages.END_DATE_BEFORE_START_DATE in response.content.decode()

    def test_add_course_participants(self):
        data = {'participants_ids': [self.student_1.id, self.student_2.id]}
        url = self.courses_detail_url.format(id=self.course_1.id)
        response = self.client.patch(url, data=data, format="json")
        course_participants = CourseParticipant.objects.filter(course_id=self.course_1.id)
        assert response.status_code == status.HTTP_200_OK
        assert course_participants.count() == 2
        assert CourseParticipant.objects.filter(course_id=self.course_1.id, student_id=self.student_1.id).exists()
        assert CourseParticipant.objects.filter(course_id=self.course_1.id, student_id=self.student_2.id).exists()

    def test_remove_course_participant(self):
        CourseParticipant.objects.create(course_id=self.course_1.id, student_id=self.student_1.id)
        CourseParticipant.objects.create(course_id=self.course_1.id, student_id=self.student_2.id)
        data = {'participants_ids': [self.student_1.id]}
        url = self.courses_detail_url.format(id=self.course_1.id)
        response = self.client.patch(url, data=data, format="json")
        course_participants = CourseParticipant.objects.filter(course_id=self.course_1.id)
        assert response.status_code == status.HTTP_200_OK
        assert course_participants.count() == 1
        assert CourseParticipant.objects.filter(course_id=self.course_1.id, student_id=self.student_1.id).exists()
        assert not CourseParticipant.objects.filter(course_id=self.course_1.id, student_id=self.student_2.id).exists()

    def test_delete_course(self):
        url = self.courses_detail_url.format(id=self.course_1.id)
        response = self.client.delete(url, kwargs={"id": self.course_1.id}, format="json")
        deleted_course = Course.objects.filter(id=self.course_1.id).first()
        existing_course = Course.objects.filter(id=self.course_2.id).first()
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert not deleted_course
        assert existing_course


class IsCourseParticipantServiceTestCase(TestCase):
    def setUp(self):
        self.course_1 = Course.objects.create(name="Advanecd Physics", start_date="2018-01-03", end_date="2018-03-03")
        self.course_2 = Course.objects.create(name="C++ in 21 day", start_date="2019-04-01", end_date="2021-04-01")
        self.student_1 = Student.objects.create(first_name="John", last_name="Doe")
        CourseParticipant.objects.create(student=self.student_1, course=self.course_1)

    def test_student_is_participant_of_course(self):
        assert is_participant_of_course(self.student_1.id, self.course_1.id)

    def test_student_is_not_participant_of_course(self):
        assert not is_participant_of_course(self.student_1.id, self.course_2.id)
