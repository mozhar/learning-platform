from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from .serializers import CourseSerializer, CoursesListSerializer
from .services import get_courses_with_students_count


class CoursesViewSet(mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet):

    queryset = get_courses_with_students_count()

    def get_serializer_class(self):
        if self.action == 'list':
            return CoursesListSerializer
        return CourseSerializer
