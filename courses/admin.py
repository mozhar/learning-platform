from django.contrib import admin

from .models import Course, CourseParticipant


class CourseParticipantInline(admin.StackedInline):
    model = CourseParticipant
    raw_id_fields = ("student",)


class CourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'students_count', 'start_date', 'end_date')
    inlines = [
        CourseParticipantInline,
    ]

    def students_count(self, instance):
        return instance.participants.count()


admin.site.register(Course, CourseAdmin)
