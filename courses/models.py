from django.db import models

from students.services import get_student_model

Student = get_student_model()


class Course(models.Model):
    name = models.CharField(max_length=128, verbose_name="Name")
    description = models.TextField(max_length=128, blank="true", verbose_name="Description")
    start_date = models.DateField()
    end_date = models.DateField()

    participants = models.ManyToManyField(Student, through='CourseParticipant')

    class Meta:
        verbose_name = 'Course'
        verbose_name_plural = 'Courses'
        db_table = 'course'

    def __str__(self):
        return self.name


class CourseParticipant(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    completed = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Course participant'
        verbose_name_plural = 'Course participants'
        db_table = 'course_participant'
