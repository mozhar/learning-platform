### Installation

This project requires python 3.5+.  

##### Steps
1. Create a virtual env and install the modules requirements (listed in `requirements.txt`).
2. Migrate the database (`./manage.py migrate`).
3. Optionally you can load fixtures to database (`./manage.py loaddata fixtures/sample_data.json
`).  

That's it - no other dependencies like DBMS or docker, it's just a basic django project.

### Testing

To run tests use `pytest` command.  
To run feature scenarios (behavioral tests) use `./manage.py behave`.

### Docs

##### Api docs
Api docs is available at the root url of project (swagger).