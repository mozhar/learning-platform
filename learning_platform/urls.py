from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='API Docs')

urlpatterns = [
    path('', schema_view),

    path('admin/', admin.site.urls),
    path('courses/', include('courses.urls')),
    path('students/', include('students.urls')),
]
