from datetime import datetime
from urllib.parse import quote

from django.http import FileResponse


def get_current_date_string():
    return datetime.today().strftime('%Y-%m-%d')


def get_file_response(file_content, file_name):
    response = FileResponse(file_content)
    response['Content-Disposition'] = f'attachment; filename={quote(file_name)};'
    response['Content-Type'] = 'application/octet-stream'
    return response
