from behave import then


@then('response code will be {code}')
def check_response_code(context, code):
    context.test.assertEquals(code, str(context.response.status_code))
