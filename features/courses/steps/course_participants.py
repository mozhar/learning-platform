from behave import given, then, when
from django.urls import reverse
from rest_framework.test import APIClient

from courses.models import Course, CourseParticipant
from students.models import Student

from features.utils import table_to_list


# TODO maybe student/course setup steps should be moved somewhere else

@given('course {course_name}')
def create_course(context, course_name):
    context.course = Course.objects.create(name=course_name, start_date="2020-01-03", end_date="2020-02-03")


@given('student {first_name} {last_name}')
def create_student(context, first_name, last_name):
    context.student = Student.objects.create(first_name=first_name, last_name=last_name)


@given('the set of students')
def create_set_of_students(context):
    context.students = []
    for student in table_to_list(context.table):
        context.students.append(
            Student.objects.create(**student)
        )

@when('user adds this student to this course through API')
def add_student_to_course_through_api(context):
    data = {'participants_ids': context.student.id}
    # url = reverse('courses', kwargs={"id": context.course.id}) # fixme
    url = f'/courses/{context.course.id}/'
    context.response = APIClient().patch(path=url, data=data)


@when('user adds this students to this course through API')
def add_set_of_students_to_course(context):
    data = {'participants_ids': [s.id for s in context.students]}
    # url = reverse('courses', kwargs={"id": context.course.id}) # fixme
    url = f'/courses/{context.course.id}/'
    context.response = APIClient().patch(path=url, data=data)


@then('student is now participant of this course')
def check_student_is_participant_of_course(context):
    context.test.assertTrue(
        CourseParticipant.objects.filter(
            course_id=context.course.id,
            student_id=context.student.id
        ).exists()
    )


@then('all students are now participants of the course')
def check_all_students_are_participants_of_course(context):
    course_participants_ids = context.course.participants.values_list('id', flat=True)
    context.test.assertTrue(
        all(
            [student.id in course_participants_ids
             for student in context.students]
        )
    )


@then('course is marked as not completed for student')
def check_course_is_not_completed_for_student(context):
    course_participant = CourseParticipant.objects.get(course_id=context.course.id, student_id=context.student.id)
    context.test.assertFalse(course_participant.completed)
