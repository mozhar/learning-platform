Feature: Adding and removing participants from course

  Scenario: User adds participant to course through API
    Given course Advanced Physics
    And student John Doe
    When user adds this student to this course through API
    Then response code will be 200
    And student is now participant of this course
    And course is marked as not completed for student

  Scenario: User adds multiple participans to course through API
    Given course Advanced Physics
    And the set of students
      | first_name  | last_name |
      | John        | Doe       |
      | Dohn        | Joe       |
      | Phoebe      | Buffay    |
    When user adds this students to this course through API
    Then response code will be 200
    And all students are now participants of the course