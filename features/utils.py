def table_to_list(table):
    return [dict(zip(table.headings, row)) for row in table]
